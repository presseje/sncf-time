#!/usr/bin/env lua

local http = require("socket.http")
local url_encode = require("socket.url").escape
local ltn12 = require("ltn12")
local json = require("json")
local inifile = require("inifile")
local argparse = require("argparse")

local config_path = os.getenv("HOME").."/.sncf_time_config"
local function init_config()
    local res, config = pcall(function()
        return inifile.parse(config_path)
    end);
    if not res or not config then config = {} end
    return config
end

local function init_args()
    local parser = argparse(arg[-1]..arg[0], "A tool to check the next trains at a SNCF station", "For more info, see README.md")
    parser:argument("command", [['find' : Interactive search for a station
'list' : List configured stations
'next' : Show next departures from station
'config' : Show and modify config]])
        :args("?")
        --TODO restrict choices
    parser:option("-t --token", "Your SNCF API token")
    parser:option("-p --proxy", "Proxy URL")
    parser:option("-f --from", "Departure station alias")
    parser:option("-t --to", "Destination station alias")
    parser:flag("-s --save", "Save proxy settings and stations aliases to "..config_path)
    parser:flag("-v --verbose", "Show the API exchanges")
    --TODO Add bash completion generator

    return parser:parse(), function(err_msg) parser:error(err_msg) end
end

local config = init_config()
local args, argerror = init_args()

if not args.token then
    argerror("no API token")
end

local function printf(msg, ...)
    print(string.format(msg, ...))
end
local function vprintf(msg, ...)
    if args.verbose then printf("[debug] "..msg, ...) end
end
local function parse_date(isodate)
    local hh,mm,ss = isodate:match("T(%d%d)(%d%d)(%d%d)$")
    return string.format("%s:%s%s", hh, mm, ss ~= "00" and "."..ss or "")
end

local function do_api_call(req, params)
    local url = "https://api.sncf.com/v1/coverage/sncf/" .. req
    if params then
        local first_param = true
        for k,v in pairs(params) do
            url = url .. (first_param and "?" or "&"); first_param = false
            url = url .. k.."="..url_encode(v)
        end
    end
    vprintf("Calling '%s'", url)
    local chunks = {}
    local _,code = http.request {
        url = url,
        method = "GET",
        headers = {
            Authorization = args.token,
        },
        proxy = config.user.proxy,
        redirect = false,
        sink = ltn12.sink.table(chunks),
    }
    vprintf("Answser : HTTP %d", code)
    return json.decode(table.concat(chunks))
end

local function find_station()
    local result
    while not result do
        local station
        while not station or #station == 0 do
            io.write("Enter station name > ")
            station = io.read("*line")
        end
        local resp = do_api_call("places", {
            q = station,
            ["type[]"] = "stop_area",
        })
        if resp.places then
            for i,stop_area in ipairs(resp.places) do
                printf(" [%02d] %s", i, stop_area.name)
            end
            io.write("Choose number or retry > ")
            local selected_id = math.floor(tonumber(io.read("*line")) or 0)
            if selected_id > 0 then
                result = resp.places[selected_id].id
            end
        end
    end
    return result
end

local function choose_alias()
    local alias
    while not alias or #alias == 0 do
        io.write("Choose alias > ")
        alias = io.read("*line")
    end
    return alias
end

if args.command then
    if args.command == "find" then
        local stop_id = find_station()
        local alias = choose_alias()
        if not config.aliases then
            config.aliases = {}
        end
        config.aliases[alias] = stop_id
    end
    if args.command == "list" then
        if config.aliases then
            for alias,id in pairs(config.aliases) do
                printf("%s : '%s'", alias, id)
            end
        end
    end
    if args.command == "next" then
        if not args.from then
            argerror("Please specify departure station alias")
        end
        local from_id = config.aliases[args.from]
        if not from_id then
            argerror("Unknown station alias "..args.from)
        end
        local resp = do_api_call("stop_areas/"..from_id.."/departures", {
            count = 5,
            data_freshness  = "realtime",
        })
        for _,dep in ipairs(resp.departures) do
            local disp = dep.display_informations
            local dates = dep.stop_date_time
            printf ("%s [%s %s%s] %s",
                parse_date(dates.departure_date_time),
                disp.network,
                disp.code,
                disp.headsign and " - "..disp.headsign or "",
                disp.direction
            )
        end
    end
end

if args.save then
    printf("Saving config to "..config_path)
    inifile.save(config_path, config)
end
